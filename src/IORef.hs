module Main where

import Control.Monad
import qualified Data.Map.Strict as Map
import Data.IORef

type AccountId = Int
type Dollars = Integer

type Account = IORef Dollars

type Bank = IORef (Map.Map AccountId Account)

makeAccount :: Bank -> AccountId -> IO Account
makeAccount bank aid = do
    account <- newIORef 0
    modifyIORef bank $ Map.insert aid account
    return account

getAccount :: Bank -> AccountId -> IO Account
getAccount bank aid = do
    accounts <- readIORef bank
    case Map.lookup aid accounts of
        Just account -> return account
        Nothing -> makeAccount bank aid

main :: IO ()
main = do
    bank <- newIORef Map.empty

    account <- makeAccount bank 1000
    account' <- getAccount bank 5

    summary <- do
        accounts <- readIORef bank
        forM (Map.assocs accounts) $ \(aid, account) -> do
            balance <- readIORef account
            return (aid, balance)

    print summary
