-- Via https://stackoverflow.com/a/10100520
module Main where

import Control.Concurrent.STM
import Control.Monad
import qualified Data.Map.Strict as Map

type AccountId = Int

type Dollars = Integer

type Account = TVar Dollars

type Bank = TVar (Map.Map AccountId Account)

-- Get account by ID, create new empty account if it didn't exist
makeAccount :: Bank -> AccountId -> STM Account
makeAccount bank accountId = do
    account <- newTVar 0
    modifyTVar bank $ Map.insert accountId account
    return account

-- Get account by ID, create new empty account if it didn't exist
getAccount :: Bank -> AccountId -> STM Account
getAccount bank accountId = do
    accounts <- readTVar bank
    case Map.lookup accountId accounts of
        Just account -> return account
        Nothing -> do
            account <- newTVar 0
            writeTVar bank $ Map.insert accountId account accounts
            return account

-- Transfer amount between two accounts (accounts can go negative)
transfer :: Dollars -> Account -> Account -> STM ()
transfer amount from to =
    when (from /= to) $ do
        balanceFrom <- readTVar from
        balanceTo <- readTVar to
        writeTVar from $ balanceFrom - amount
        writeTVar to $ balanceTo + amount

main = do
    bank <- newTVarIO (Map.empty :: Map.Map AccountId Account)

    atomically $ makeAccount bank 10
    atomically $ getAccount bank 5

    summary <- atomically $ do
        accounts <- readTVar bank
        forM (Map.assocs accounts) $ \(accountId, account) -> do
            balance <- readTVar account
            return (accountId, balance)

    print summary
