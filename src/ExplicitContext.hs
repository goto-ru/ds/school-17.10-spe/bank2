module Main where

import qualified Data.Map.Strict as Map

type AccountId = Int
type Dollars = Integer

type Account = Dollars

type Bank = Map.Map AccountId Account

-- Explicit context passing to work with immutability
-- Does not compose well: we want Bank in implicit context

makeAccount :: Bank -> AccountId -> Bank
makeAccount bank aid = Map.insert aid 0 bank

-- Get account by ID, create new empty account if it didn't exist
--getAccount = undefined
getAccount :: Bank -> AccountId -> Account
getAccount bank aid = case Map.lookup aid bank of
    Just account -> account
    Nothing -> undefined -- ???

main :: IO ()
main = do
    let bank = Map.empty :: Bank
    let bank' = makeAccount bank 1
    print bank'
    print $ Map.lookup 3 bank'
